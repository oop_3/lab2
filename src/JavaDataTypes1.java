public class JavaDataTypes1 {
    public static void main(String[] args) throws Exception {
        int Num = 5;               // Integer (whole number)
        float FloatNum = 5.99f;    // Floating point number
        char char_ = 'D';         // Character
        boolean Bool = true;       // Boolean
        String Text = "Hello";     // String
    }
    
}
