public class JavaTypeCasting2 {
    public static void main(String[] args) throws Exception {
        int num = 9;
        double do_ = num; // Automatic casting: int to double
        System.out.println(num);
        System.out.println(do_);

    }
    
}
