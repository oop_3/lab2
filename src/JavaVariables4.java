public class JavaVariables4 {
    public static void main(String[] args) throws Exception {
        final int num = 20;
        //num = 25; // error cuse cannot assign a value to a final variable
    }
    
}
