public class JavaTypeCasting1 {
    public static void main(String[] args) throws Exception {
        double double_  = 1.5;
        int int_  = (int)double_; // Manual casting: double to int
        System.out.println(double_);
        System.out.println(int_);
    }
    
}
